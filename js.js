
$(window).scroll(()=>{
    if ($(this).scrollTop() > 400) {
        $('#scroll-to-top').css("opacity", "1")
    } else {
        $('#scroll-to-top').css("opacity", "0")
    }

})


function postData() {
    const url = 'https://test-fetch-jp.requestcatcher.com/',
        form = document.querySelector("form"),
        modalFinish = document.querySelector(".modal-finish"),
        inputs = document.querySelectorAll("input")


    const clearInputs = () => {
        inputs.forEach(input => {
            input.value = ""
        })
    }

    form.onsubmit = async (e) => {
        e.preventDefault();
        $("#modal").modal("hide");
        modalFinish.style.display = "block"
        document.body.style.overflow = "hidden"
        let statusMessage = document.createElement("h3");
        statusMessage.textContent = "Ваша заявка отправлена. Мы Вам перезвоним"
        modalFinish.appendChild(statusMessage);
        clearInputs()

        setTimeout(() => {
            statusMessage.remove()
            modalFinish.style.display = "none";
            document.body.style.overflow = "";
        }, 4000);


        // let response = await fetch(url, {
        //     method: 'POST',
        //     body: new FormData(form)
        // });
        //
        //
        // let result = await response.json();
        //
        //
        // console.log(`ответ от сервера ${result}`);



        let user = {
            name: 'John',
            surname: 'Smith'
        };

        let response = await fetch('https://test-fetch-jp.requestcatcher.com', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(new FormData(form))
        });

        let result = await response.json();
        console.log(`ответ от сервера ${result}`);



    };


}

postData()


